Provides extra coloring options for the Fullcalendar module.
With this module it is possible to link any classname to a color in the UI.

To use the fullcalendar colors extension module you only need to install
fullcalendar, with the color submodule and all his dependencies.

You can find the color options under the fullcalendar color settings.

Enjoy coloring :)
