<?php

/**
 * @file
 * Color page callbacks for the Fullcalendar colors extension module.
 */

/**
 * Configuration form for coloring classnames.
 *
 * @see fullcalendar_colors_extension_admin_add_classname_validate()
 * @see fullcalendar_colors_extension_admin_add_classname_submit()
 * @see fullcalendar_colors_extension_admin_update_colors_submit()
 */
function fullcalendar_colors_extension_admin_classnames() {

  module_load_include('inc', 'fullcalendar_colors_extension', 'includes/fullcalendar_colors_extension.database');
  module_load_include('inc', 'fullcalendar_colors', 'includes/fullcalendar_colors.admin');

  ctools_add_css('fullcalendar_colors_extension.admin', 'fullcalendar_colors_extension');
  $form = _load_colorpicker();

  $form['custom_colors'] = array(
    '#type' => 'item',
    '#title' => t('Custom Colors'),
    '#description' => t('Create your own custom colors by adding a classname and choosing a color for that name.'),
  );
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#title_display' => 'invisible',
    '#size' => 32,
    '#maxlength' => 64,
  );
  $form['add'] = array(
    '#type' => 'submit',
    '#value' => t('Add classname'),
    '#validate' => array('fullcalendar_colors_extension_admin_add_classname_validate'),
    '#submit' => array('fullcalendar_colors_extension_admin_add_classname_submit'),
  );
  $form['classnames'] = array(
    '#tree' => TRUE,
  );
  $result = _fullcalendar_colors_extension_classnames();
  foreach ($result as $record) {

    // Get the color for the current $classname
    $classname = $record->name;
    $color = $record->color;

    $form['classnames'][$classname] = array(
      '#title' => t($classname),
      '#type' => 'textfield',
      '#title_display' => 'invisible',
      '#attributes' => array('class' => array('colorpicker-input')),
      '#default_value' => $color,
      '#size' => 7,
      '#maxlength' => 7,
    );
  }
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save colors'),
    '#submit' => array('fullcalendar_colors_extension_admin_update_colors_submit')
  );
  return $form;
}

/**
 * Form submit handler for the fullcalendar_colors_extension_admin_classname() form.
 */
function fullcalendar_colors_extension_admin_update_colors_submit($form, &$form_state) {
  module_load_include('inc', 'fullcalendar_colors_extension', 'includes/fullcalendar_colors_extension.database');
  foreach ($form_state['values']['classnames'] as $classname => $color) {
    _fullcalendar_colors_extension_update_color_classname($classname, $color);
  }
}

/**
 * Returns HTML for the classname form.
 *
 * @param $variables
 *   An associative array containing:
 *   - form: A render element representing the form.
 */
function theme_fullcalendar_colors_extension_admin_classnames($variables) {
  $form = $variables['form'];
  $header = array(t('Classname'), t('Color'), array('data' => t('Operations'), 'colspan' => 2));

  $rows = array();
  foreach (element_children($form['classnames']) as $class) {
    $row = array();
    $row[] = check_plain($form['classnames'][$class]['#title']);
    $row[] = drupal_render($form['classnames'][$class]);
    $row[] = l(t('edit classname'), 'admin/config/calendar/fullcalendar/colors/classname/edit/' . $class);
    $row[] = l(t('delete classname'), 'admin/config/calendar/fullcalendar/colors/classname/delete/' . $class);

    $rows[] = array('data' => $row);
  }
  $rows[] = array(array('data' => drupal_render($form['name']) . drupal_render($form['add']), 'colspan' => 4, 'class' => 'edit-name'));

  $output = drupal_render($form['color_picker']);
  $output .= drupal_render($form['custom_colors']);
  $output .= theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => 'fullcalender-colors-extension-classnames')));
  $output .= drupal_render($form['actions']);
  $output .= drupal_render($form['form_build_id']);
  $output .= drupal_render($form['form_token']);
  $output .= drupal_render($form['form_id']);

  return $output;
}

/**
 * Form validation handler for the fullcalendar_colors_extension_admin_classname() form.
 */
function fullcalendar_colors_extension_admin_add_classname_validate($form, &$form_state) {
  module_load_include('inc', 'fullcalendar_colors_extension', 'includes/fullcalendar_colors_extension.database');
  if (!empty($form_state['values']['name'])) {
    if (_fullcalendar_colors_extension_color_classname($form_state['values']['name'])) {
      form_set_error('name', t('The classname %name already exists. Choose another classname.', array('%name' => $form_state['values']['name'])));
    }
  }
  else {
    form_set_error('name', t('You must specify a valid classname.'));
  }
}

/**
 * Form submit handler for the fullcalendar_colors_extension_admin_classname() form.
 */
function fullcalendar_colors_extension_admin_add_classname_submit($form, &$form_state) {
  module_load_include('inc', 'fullcalendar_colors_extension', 'includes/fullcalendar_colors_extension.database');
  _fullcalendar_colors_extension_insert_classname($form_state['values']['name']);
  drupal_set_message(t('The classname has been added.'));
}

/**
 * Form to configure a single classname.
 *
 * @see fullcalendar_colors_extension_admin_save_classname_validate()
 * @see fullcalendar_colors_extension_admin_save_classname_submit()
 * @see fullcalendar_colors_extension_admin_delete_classname_submit
 */
function fullcalendar_colors_extension_admin_classname($form, $form_state, $classname) {

  // Display the edit role form.
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Classname'),
    '#default_value' => $classname,
    '#size' => 32,
    '#required' => TRUE,
    '#maxlength' => 64,
  );
  $form['classname'] = array(
    '#type' => 'value',
    '#value' => $classname,
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save classname'),
    '#validate' => array('fullcalendar_colors_extension_admin_save_classname_validate'),
    '#submit' => array('fullcalendar_colors_extension_admin_save_classname_submit'),
  );
  $form['actions']['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete classname'),
    '#submit' => array('fullcalendar_colors_extension_admin_delete_classname_submit'),
  );

  return $form;
}

/**
 * Form validation handler for the fullcalendar_colors_extension_admin_classname() form.
 */
function fullcalendar_colors_extension_admin_save_classname_validate($form, &$form_state) {
  module_load_include('inc', 'fullcalendar_colors_extension', 'includes/fullcalendar_colors_extension.database');
  if (!empty($form_state['values']['name'])) {
    $color = _fullcalendar_colors_extension_color_classname($form_state['values']['name']);
    if ($color && $form_state['values']['classname'] != $form_state['values']['name']) {
      form_set_error('name', t('The classname %name already exists. Choose another classname.', array('%name' => $form_state['values']['name'])));
    }
  }
  else {
    form_set_error('name', t('You must specify a valid classname.'));
  }
}

/**
 * Form submit handler for the fullcalendar_colors_extension_admin_classname() form.
 */
function fullcalendar_colors_extension_admin_save_classname_submit($form, &$form_state) {
  module_load_include('inc', 'fullcalendar_colors_extension', 'includes/fullcalendar_colors_extension.database');
  _fullcalendar_colors_extension_rename_classname($form_state['values']['classname'], $form_state['values']['name']);
  drupal_set_message(t('The classname has been renamed.'));
  $form_state['redirect'] = 'admin/config/calendar/fullcalendar/colors/classname/';
}

/**
 * Form submit handler for the fullcalendar_colors_extension_admin_classname() form.
 */
function fullcalendar_colors_extension_admin_delete_classname_submit($form, &$form_state) {
  $form_state['redirect'] = 'admin/config/calendar/fullcalendar/colors/classname/delete/' . $form_state['values']['classname'];
}

/**
 * Form to confirm classname delete operation.
 *
 * @see fullcalendar_colors_extension_admin_delete_classname_confirm_submit()
 */
function fullcalendar_colors_extension_admin_delete_classname_confirm($form, &$form_state, $classname) {
  $form['classname'] = array(
    '#type' => 'value',
    '#value' => $classname,
  );
  return confirm_form($form, t('Are you sure you want to delete the classname %name ?', array('%name' => $classname)), 'admin/config/calendar/fullcalendar/colors/classname', t('This action cannot be undone.'), t('Delete'));
}

/**
 * Form submit handler for fullcalendar_colors_extension_admin_classname_delete_confirm().
 */
function fullcalendar_colors_extension_admin_delete_classname_confirm_submit($form, &$form_state) {
  module_load_include('inc', 'fullcalendar_colors_extension', 'includes/fullcalendar_colors_extension.database');
  _fullcalendar_colors_extension_delete_classname($form_state['values']['classname']);
  drupal_set_message(t('The classname has been deleted.'));
  $form_state['redirect'] = 'admin/config/calendar/fullcalendar/colors/classname';
}

/**
 * Configuration form for color settings.
 */
function fullcalendar_colors_extension_admin_settings() {

  module_load_include('inc', 'fullcalendar_colors', 'includes/fullcalendar_colors.admin');

  ctools_add_css('fullcalendar_colors_extension.admin', 'fullcalendar_colors_extension');
  $form = _load_colorpicker();

  $form['default_color'] = array(
    '#type' => 'item',
    '#title' => t('Default Color'),
  );
  $form['default_color_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Change the default color.'),
    '#default_value' => variable_get('fullcalendar_color_extension_default_color_enabled', FALSE),
  );
  $form['default_color_input'] = array(
    '#title' => t('default color'),
    '#type' => 'textfield',
    '#attributes' => array('class' => array('colorpicker-input')),
    '#default_value' => variable_get('fullcalendar_color_extension_default_color', '#3366CC'),
    '#size' => 7,
    '#maxlength' => 7,
    '#title_display' => 'invisible',
    '#states' => array(
      'visible' => array(
        ':input[name="default_color_enabled"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['info'] = array(
    '#type' => 'item',
    '#title' => t('Process order'),
  );
  $form['process_order_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Change the css processing order.'),
    '#default_value' => variable_get('fullcalendar_color_extension_process_order_enabled', FALSE),
    '#description' => t('Save settings after enabling/disabling this feature.'),
  );

  $modules = array();
  $hook = 'fullcalendar_colors_create_css';
  foreach (module_implements($hook) as $module) {
    $modules[$module] = variable_get('fullcalendar_colors_extension_weight_' . $module, 0);
  }
  asort($modules);

  $form['modules'] = array(
    '#tree' => TRUE,
  );
  foreach ($modules as $module => $weight) {

    // Get the module name
    $path = drupal_get_path('module', $module) . '/' . $module . '.info';
    $info = drupal_parse_info_file($path);

    $form['modules'][$module]['#name'] = $info['name'];
    $form['modules'][$module]['#weight'] = $weight;
    $form['modules'][$module]['weight'] = array(
      '#type' => 'textfield',
      '#title' => t('Weight for @title', array('@title' => $module)),
      '#title_display' => 'invisible',
      '#size' => 4,
      '#default_value' => $weight,
      '#attributes' => array('class' => array('role-weight')),
    );
  }
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save settings'),
    '#submit' => array('fullcalendar_colors_extension_admin_settings_submit')
  );
  
  return $form;
}

/**
 * Form submit handler for the fullcalendar_colors_extension_admin_classname() form.
 */
function fullcalendar_colors_extension_admin_settings_submit($form, &$form_state) {

  if ($form_state['values']['default_color_enabled']) {
    variable_set('fullcalendar_color_extension_default_color', $form_state['values']['default_color_input']);
    variable_set('fullcalendar_color_extension_default_color_enabled', TRUE);
  }
  else
    variable_set('fullcalendar_color_extension_default_color_enabled', FALSE);
    
  variable_set('fullcalendar_color_extension_process_order_enabled', $form_state['values']['process_order_enabled']);

  foreach ($form_state['values']['modules'] as $module => $weight) {
    if ($form['modules'][$module]['#weight'] != $weight['weight']) {
      variable_set('fullcalendar_colors_extension_weight_' . $module, $weight['weight']);
    }
  }
}

/**
 * Returns HTML for the settings form.
 *
 * @param $variables
 *   An associative array containing:
 *   - form: A render element representing the form.
 */
function theme_fullcalendar_colors_extension_admin_settings($variables) {
  $form = $variables['form'];
  $header = array(t('Module'), t('weight'));

  $rows = array();
  foreach (element_children($form['modules']) as $module) {
    $row = array();
    $row[] = check_plain($form['modules'][$module]['#name']);
    $row[] = drupal_render($form['modules'][$module]['weight']);

    $rows[] = array('data' => $row, 'class' => array('draggable'));
  }

  drupal_add_tabledrag('fullcalender-colors-extension-settings', 'order', 'sibling', 'role-weight');

  $output = drupal_render($form['color_picker']);
  $output .= drupal_render($form['default_color']);
  $output .= drupal_render($form['default_color_enabled']);
  $output .= drupal_render($form['default_color_input']);
  $output .= drupal_render($form['info']);
  $output .= drupal_render($form['process_order_enabled']);
  if (variable_get('fullcalendar_color_extension_process_order_enabled', FALSE))
    $output .= theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => 'fullcalender-colors-extension-settings')));
  $output .= drupal_render($form['actions']);
  $output .= drupal_render($form['form_build_id']);
  $output .= drupal_render($form['form_token']);
  $output .= drupal_render($form['form_id']);

  return $output;
}
