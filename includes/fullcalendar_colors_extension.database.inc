<?php

/**
 * @file
 * Database functions for the Fullcalendar colors extension module.
 */

/**
 * Gets an array of all the custom classnames.
 *
 * @return
 *	 Array containing all the custom classnames.
 */
function _fullcalendar_colors_extension_classnames() {
  $result = db_select('fullcalendar_colors_classname', 'c')
    ->fields('c')
    ->execute();

  return $result;
}

/**
 * Gets the color of a given classname.
 *
 * @param $classname
 *	 The name of the class.
 *
 * @return
 *	 Color of the given classname
 */
function _fullcalendar_colors_extension_color_classname($classname) {
  $result = db_select('fullcalendar_colors_classname', 'c')
    ->fields('c', array('color'))
    ->condition('name', $classname, '=')
    ->execute()
    ->fetchAssoc();

  return $result;
}

/**
 * Inserts a new classname.
 *
 * @param $classname
 *	 The name of the class.
 */
function _fullcalendar_colors_extension_insert_classname($classname) {
  db_insert('fullcalendar_colors_classname')
    ->fields(array(
      'name' => $classname,
    ))
    ->execute();
}

/**
 * Updates the color for a given classname.
  *
 * @param $classname
 *	 The name of the class.
 * @param $color
 *   The updated color.
 */
function _fullcalendar_colors_extension_update_color_classname($classname, $color) {
  db_update('fullcalendar_colors_classname')
    ->fields(array(
      'color' => $color,
    ))
    ->condition('name', $classname, '=')
    ->execute();
}

/**
 * Deletes a classname.
 *
 * @param $classname
 *	 The classname to remove.
 */
function _fullcalendar_colors_extension_delete_classname($classname) {
  db_delete('fullcalendar_colors_classname')
    ->condition('name', $classname)
    ->execute();
}

/**
 * Renames a given classname.
 *
 * @param $oldclassname
 *	 The name of the given class.
 * @param $oldclassname
 *   The new name for the class.
 */
function _fullcalendar_colors_extension_rename_classname($oldclassname, $newclassname) {
  db_update('fullcalendar_colors_classname')
    ->fields(array(
      'name' => $newclassname,
    ))
    ->condition('name', $oldclassname, '=')
    ->execute();
}
